package SeleniumCucumber.Helper;
import SeleniumCucumber.Operations;
import SeleniumCucumber.PageObjects.SaucePageObj;
import SeleniumCucumber.Utility.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Login extends Operations{

    private WebDriver _driver;
    private SaucePageObj _saucePageobj;

    public Login(WebDriver driver) {
        super(driver);
        this._driver = driver;
        _saucePageobj = new SaucePageObj(_driver);
    }

    public void navigate() throws IOException {
        Properties prop = Utilities.ReadData();
        _driver.manage().window().maximize();
        _driver.get(prop.getProperty("url"));
        implicitWait(20);
    }

    public void enterCreds(String user) throws Exception {
        Properties prop = Utilities.ReadData();
        String usercreds = prop.getProperty(user);
        String userName = Utilities.SplitText1WithSpace(usercreds);
        String passWord = Utilities.SplitText2WithSpace(usercreds);

        EnterText(_saucePageobj.username,userName);
        implicitWait(20);
        EnterText(_saucePageobj.password, passWord);
        implicitWait(20);
        Clicks(_saucePageobj.loginButton);
    }

    public void addProductInCart() throws Exception
    {
        implicitWait(20);
        waitForElementDisplayed(_saucePageobj.product1);
        moveToElementAndClicks(_saucePageobj.product1);
        implicitWait(20);
    }

    public void addProduct2InCart() throws Exception
    {
        waitForElementDisplayed(_saucePageobj.product2);
        implicitWait(20);
        moveToElementAndClicks(_saucePageobj.product2);
    }

    public void verifyProductsInCart(String prod1, String prod2) throws Exception
    {
        implicitWait(20);
        moveToElementAndClicks(_saucePageobj.cart);
        String addedProd1 = _saucePageobj.cartProduct1.getText();
        String addedProd2 = _saucePageobj.cartProduct2.getText();

        Assert.assertEquals(addedProd1, prod1);
        Assert.assertEquals(addedProd2, prod2);
    }

    public String addRandomProductToTheCart() throws Exception
    {
        moveToElementAndClicks((_saucePageobj.continueShopping));
        List<WebElement> productList = _saucePageobj.allProducts;
        List<WebElement> productTextList = _saucePageobj.allProducttext;
        int randomValue = Utilities.selectRandomValue(0,productList.size()-1);
        String addedProductText = productTextList.get(randomValue).getText();
        moveToElementAndClicks(productList.get(randomValue));

        return addedProductText;
    }

    public void verifyNewlyAddedProductInCart(String productText) throws Exception {
        moveToElementAndClicks(_saucePageobj.cart);
        Boolean flag = _driver.findElement(By.xpath("//div[contains(text(), '"+productText+"')]")).isDisplayed();
        Assert.assertTrue(flag,"selected product is not added to the cart");
    }
}
