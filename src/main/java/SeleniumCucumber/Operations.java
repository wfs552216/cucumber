package SeleniumCucumber;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Operations {

    private WebDriver _driver;

    public Operations(WebDriver driver) {
        _driver = driver;
    }

    public void Clicks(WebElement element) throws Exception {
        try
        {
            element.click();
        }
        catch (Exception e)
        {
            throw new Exception("Element: Not Found.");
        }
    }

    public void EnterText(WebElement element, String value) throws Exception {
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            throw new Exception("Element Not Found : ");
        }
    }

    public void moveToElement(WebElement element) throws Exception {
        try
        {
            JavascriptExecutor executor = (JavascriptExecutor) _driver;
            executor.executeScript("arguments[0].scrollIntoView(true);", element);
        }
        catch (Exception e)
        {
            throw new Exception("Element is not located");
        }
    }

    public void moveToElementAndClicks(WebElement element) throws Exception {
        try
        {
            moveToElement(element);
            Clicks(element);
        }
        catch (Exception e)
        {
            throw new Exception("Element is not located or click operation failed");
        }
    }

    public WebElement waitForElementDisplayed(WebElement element) throws Exception {
        try
        {
            var wait = new WebDriverWait(_driver, Duration.ofSeconds(20));
            wait.until(ExpectedConditions.visibilityOf(element));
            return element;
        }
        catch (Exception e)
        {
            throw new Exception(element + " Not displayed on UI");
        }
    }

    public void implicitWait(int time)
    {
        _driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
    }
}
