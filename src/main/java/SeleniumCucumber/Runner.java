package SeleniumCucumber;

import SeleniumCucumber.Hooks.Hooks;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

public class Runner {

    @CucumberOptions(tags = "", features = {"src/test/resources/features/LoginPage.feature"}, glue = {"Seleniumcucumber.definitions", "Hooks"},
            plugin = {})

    public class CucumberRunnerTests extends AbstractTestNGCucumberTests {

    }
}
