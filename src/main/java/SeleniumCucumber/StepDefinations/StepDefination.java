package SeleniumCucumber.StepDefinations;
import SeleniumCucumber.Driver;
import SeleniumCucumber.Helper.Login;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;


public class StepDefination{

    private Login login = new Login(Driver.getDriver());
    public String ProductText;

    @Given("^User launches the url$")
    public void LaunchURL() throws IOException {
        login.navigate();
    }

    @When("{string} enters the credentials")
    public void entersTheCredentials(String user) throws Exception
    {
        login.enterCreds(user);
    }

    @And("User adds first product in cart")
    public void userAddsFirstProductInCart() throws Exception {
        login.addProductInCart();
    }

    @And("User adds second product in cart")
    public void userAddsSecondProductInCart() throws Exception {
        login.addProduct2InCart();
    }

    @Then("User Verifies that the products {string} and {string} are added to the cart")
    public void userVerifiesThatTheProductsAndAreAddedToTheCart(String prod1, String prod2) throws Exception
    {
        login.verifyProductsInCart(prod1,prod2);
    }

    @When("User randomly adds single product")
    public void userRandomlyAddsSingleProduct() throws Exception
    {
        ProductText = login.addRandomProductToTheCart();
    }

    @Then("User verifies that the one more product added to the cart")
    public void userVerifiesThatTheOneMoreProductAddedToTheCart() throws Exception
    {
        login.verifyNewlyAddedProductInCart(ProductText);
    }
}

