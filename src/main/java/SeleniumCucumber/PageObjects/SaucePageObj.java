package SeleniumCucumber.PageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SaucePageObj {

    private WebDriver _driver;

    public SaucePageObj(WebDriver driver)
    {
         this._driver = driver;
        PageFactory.initElements(driver, this);
    }

    //public WebElement btn = _driver.findElement(By.cssSelector("#login-button"));

    @FindBy(css = "#user-name")
    public WebElement username;

    @FindBy(css= "#login-button")
    public WebElement loginButton;

    @FindBy(css= "#password")
    public WebElement password;

    @FindBy(css= "#add-to-cart-sauce-labs-backpack")
    public WebElement product1;

    @FindBy(css= "#add-to-cart-sauce-labs-bike-light")
    public WebElement product2;

    @FindBy(id="item_4_title_link")
    public WebElement cartProduct1;

    @FindBy(id="item_0_title_link")
    public WebElement cartProduct2;

    @FindBy(css= "#shopping_cart_container")
    public WebElement cart;

    @FindBy(xpath= "//button[contains(@class, 'primary')]")
    public List<WebElement> allProducts;

    @FindBy(xpath= "//button[contains(@class, 'primary')]/..//parent::div/div//a")
    public List<WebElement> allProducttext;

    @FindBy(css= "#continue-shopping")
    public WebElement continueShopping;
}
