package SeleniumCucumber.Hooks;

import SeleniumCucumber.Driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

public class Hooks {

    private WebDriver _driver;
    private Driver driver;

    @Before
    public void BeforeScenario()
    {
         driver = new Driver();
        _driver = driver.LaunchDriver("Chrome");
    }

    @After
    public void AfterScenario()
    {
        _driver.quit();
    }
}
