package SeleniumCucumber;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver{

    public WebDriver _driver;
    public static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public WebDriver LaunchDriver(String browser)
    {
        try
        {
            switch(browser) {
                case "Chrome":
                    WebDriverManager.chromedriver().setup();
                    driver.set(new ChromeDriver());
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.println("Browser not opened");
        }
        return getDriver();
    }

    public static synchronized WebDriver getDriver()
    {
        return driver.get();
    }
}
