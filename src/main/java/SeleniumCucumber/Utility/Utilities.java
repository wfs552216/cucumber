package SeleniumCucumber.Utility;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

public class Utilities {

    public static Properties ReadData() throws IOException {
        Properties properties = new Properties();
        FileReader reader = new FileReader("src/main/java/SeleniumCucumber/Config.propertise");
        properties.load(reader);
        return properties;
    }

    public static String SplitText1WithSpace(String text)
    {
        String[] a = text.split(" ");
        return a[0];
    }

    public static String SplitText2WithSpace(String text)
    {
        String[] a = text.split(" ");
        return a[1];
    }

    public static int selectRandomValue(int start, int end)
    {
        Random rnumber = new Random();
        int random = rnumber.nextInt(start,end);
        return random;
    }
}

