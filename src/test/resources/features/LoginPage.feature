
  Feature: User wants to add the product and verify it in the cart

    @Smoke
    Scenario Outline: User verify Products in the cart
      Given User launches the url
      When "<Users>" enters the credentials
      And User adds first product in cart
      And User adds second product in cart
      Then User Verifies that the products "Sauce Labs Backpack" and "Sauce Labs Bike Light" are added to the cart
      When User randomly adds single product
      Then User verifies that the one more product added to the cart
      Examples:
        |   Users    |
        |  stduser   |
        | visualuser |
